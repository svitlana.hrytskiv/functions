const getSum = (str1, str2) => {
  if (/[a-zа-яё]/i.test(str1) && /[a-zа-яё]/i.test(str2)){
    return false
  }else if (typeof(str1) !== 'string' && typeof(str2) !== 'string'){
    return false
  }else{
    return String(Number(str1) + Number(str2))
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0
  let comments = 0;
  for(let x of listOfPosts){
    if(x.author === authorName){
      posts++;
    }
  }
  for(let y in listOfPosts){
    for(let z in listOfPosts[y].comments){
      if(listOfPosts[y].comments[z].author === authorName){
        comments++;
      }
    }
  }
  return `Post:${posts},comments:${comments}`;
};

const tickets=(people)=> {
  let changePeople = people.map(el => Number(el))
  let moneySum = 0;
  for (let personMoney of changePeople ){
    if(personMoney === 25){
      moneySum += personMoney
    }else{
      moneySum = moneySum - (personMoney - 25)
    }
    if(moneySum < 0){
      return 'NO'
    }
    moneySum += 25
  }
  if(moneySum>0){
    return 'YES'
  }
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
